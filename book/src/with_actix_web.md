# `with-actix-web` feature


Yarte implements template with `actix-web`'s `Responder` trait to make it easier for the user to incorporate templates
in this framework. This is done using feature `with-actix-web` of your `Cargo.toml`:

```toml
[build-dependencies]
yarte = { path = "../yarte", version = "0.1", features=["with-actix-web"]  }
```

For example, let's create an api with `actix-web` that will serve a template `index` when the root url is called.

The template will have no context in this case and will look this:

```handlebars
{{! Simple example !}}
{{> doc/t ~}}
<html>
{{~> doc/head title = "Actix web" ~}}
<body>
  {{~#if let Some(name) = query.get("name") }}
      {{ let lastname = query.get("lastname").ok_or(yarte::Error)? }}
  {{/if ~}}
</body></html>

```

This template will use the following struct defined in a file later refer as :

```rust
use actix_web::{Query, Responder};
use yarte::Template;

use std::collections::HashMap;

#[derive(Template)]
#[template(path = "index.hbs")]
struct IndexTemplate {
    query: Query<HashMap<String, String>>,
}

pub fn index(query: Query<HashMap<String, String>>) -> impl Responder {
    IndexTemplate { query }
}

```

Observe that public function `inedex` is the handler necessary for `actix-web` and will return a `Reponder`.

Last step is to create a simple `actix-web` with one single entry point that will serve the template created, by simply 
making reference to the function `index` created:

```rust
use actix_web::{http, middleware, server, App};

#[path = "lib.rs"]
mod template;

fn main() -> std::io::Result<()> {
    let sys = actix::System::new("example-yarte");
    std::env::set_var("RUST_LOG", "actix_web=info");
    env_logger::init();

    // start http server
    server::new(move || {
        App::new()
            .middleware(middleware::Logger::default())
            .resource("/", |r| r.method(http::Method::GET).with(template::index))
    })
    .bind("127.0.0.1:8080")?
    .start();

    println!("Started http server: 127.0.0.1:8080");
    let _ = sys.run();
    Ok(())
}

```


